using Godot;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Runtime.InteropServices;
using System.Text;

public class SmalltalkVM : Node
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";
	public MemoryMappedFile mmf;
	public MemoryMappedViewAccessor accessor;

	public Hashtable opcodes = new Hashtable();
	public Hashtable registers = new Hashtable();
	public Hashtable identities = new Hashtable();
	public int scanner = 0;
	public ArrayList routine = new ArrayList() { };
	public bool routineLoop = false;
	[Export] public bool export = false;
	[Export] public bool interpret = false;
	[Export] public bool debug = false;
	[Export(PropertyHint.File, "*.stb")] public string bytecodeFile;
	public float deltaTime = 0.0f;
	

	public const byte OPCODE_NULL = 0;
	public const byte OPCODE_WRITEREG1 = 1;
	public const byte OPCODE_WRITEREG2 = 2;
	public const byte OPCODE_WRITEREG3 = 3;
	public const byte OPCODE_WRITEREG4 = 4;
	public const byte OPCODE_WRITEREG5 = 5;
	public const byte OPCODE_READIDENT = 6;
	public const byte OPCODE_READFLOAT = 7;
	public const byte OPCODE_STRBEGIN = 8;
	public const byte OPCODE_STREND = 9;
	public const byte OPCODE_CONDBEGIN = 11;
	public const byte OPCODE_WRITECONDREG = 12;
	public const byte OPCODE_CONDEND = 14;
	public const byte OPCODE_GRTHAN = 15;
	public const byte OPCODE_GRTHANEQ = 16;
	public const byte OPCODE_LSTHAN = 17;
	public const byte OPCODE_LSTHANEQ = 18;
	public const byte OPCODE_EQUAL = 19;
	public const byte OPCODE_NOTEQ = 20;
	public const byte OPCODE_PROCBEGIN = 21;
	public const byte OPCODE_PROCEND = 22;
	public const byte OPCODE_EOF = 23;
	public const byte OPCODE_SETIDENT = 24;
	public const byte OPCODE_ADD = 25;
	public const byte OPCODE_SUB = 26;
	public const byte OPCODE_READBYTE = 27;
	public const byte OPCODE_PRINT = 50;
	public const byte OPCODE_MOVESLIDE = 51;
	public const byte OPCODE_ISPRESSED = 52;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		if (!interpret)
		{
			mmf = MemoryMappedFile.CreateOrOpen("Smalltalk5", 2000);
			/*try
			{
				mmf = MemoryMappedFile.CreateFromFile(@"C:\Users\chand\Documents\VMTest\Smalltalk.stb", System.IO.FileMode.Open, "Smalltalk4");
			}
			catch
			{
				mmf = MemoryMappedFile.CreateOrOpen("Smalltalk4", 2000);
			}*/

			accessor = mmf.CreateViewAccessor();
			GD.Print(accessor.ReadInt32(0));
        }

		opcodes.Add(OPCODE_NULL, new Op_Null());
		opcodes.Add(OPCODE_WRITEREG1, new Op_WriteRegister1());
		opcodes.Add(OPCODE_WRITEREG2, new Op_WriteRegister2());
		opcodes.Add(OPCODE_WRITEREG3, new Op_WriteRegister3());
		opcodes.Add(OPCODE_WRITEREG4, new Op_WriteRegister4());
		opcodes.Add(OPCODE_WRITEREG5, new Op_WriteRegister5());
		opcodes.Add(OPCODE_READIDENT, new Op_WriteIdentityRegisterHold());
		opcodes.Add(OPCODE_READFLOAT, new Op_WriteFloatRegisterHold());
		opcodes.Add(OPCODE_STRBEGIN, new Op_StringBegin());
		opcodes.Add(OPCODE_STREND, new Op_StringEnd());
		opcodes.Add(OPCODE_CONDBEGIN, new Op_ConditionalBegin());
		opcodes.Add(OPCODE_WRITECONDREG, new Op_WriteCondRegister());
		opcodes.Add(OPCODE_CONDEND, new Op_ConditionalEnd());
		opcodes.Add(OPCODE_GRTHAN, new Op_GreaterThan());
		opcodes.Add(OPCODE_GRTHANEQ, new Op_GreaterThanEqual());
		opcodes.Add(OPCODE_LSTHAN, new Op_LessThan());
		opcodes.Add(OPCODE_LSTHANEQ, new Op_LessThanEqual());
		opcodes.Add(OPCODE_EQUAL, new Op_Equal());
		opcodes.Add(OPCODE_NOTEQ, new Op_NotEqual());
		opcodes.Add(OPCODE_PROCBEGIN, new Op_StartRecordProcess());
		opcodes.Add(OPCODE_PROCEND, new Op_StopRecordProcess());
		opcodes.Add(OPCODE_EOF, new Op_EndOfFile());
		opcodes.Add(OPCODE_SETIDENT, new Op_SetIdentifier());
		opcodes.Add(OPCODE_ADD, new Op_Add());
		opcodes.Add(OPCODE_SUB, new Op_Subtract());
		opcodes.Add(OPCODE_READBYTE, new Op_ReadByte());
		opcodes.Add(OPCODE_PRINT, new Op_Print());
		opcodes.Add(OPCODE_MOVESLIDE, new Op_MoveAndSlide());
		opcodes.Add(OPCODE_ISPRESSED, new Op_IsPressed());

		registers["cond"] = false;

		identities[(byte)1] = (KinematicBody)GetNode("boiKinematic");
		identities[(byte)2] = 0.0f;
		identities[(byte)3] = 15.0f;
		identities[(byte)4] = 0.0f;
		if (interpret) Interpret();
	}

	public void Compute(int scanpoint)
	{
		routineLoop = false;
		var processing = false;
		var condition = false;
		var reading = false;
		var lastScanpoint = scanpoint;
		ArrayList newRoutine = new ArrayList();
		while (accessor.ReadByte(scanpoint) != 0)
		{
			var ReadByte = accessor.ReadByte(scanpoint);
			if (processing)
			{
				for (int i = lastScanpoint + 1; i < scanpoint; i++)
				{
					newRoutine.Add(accessor.ReadByte(i));
				}
				if (ReadByte != OPCODE_PROCEND)
				{
					newRoutine.Add(ReadByte);
				}
				else
				{
					processing = false;
				}
			}
			if (ReadByte == OPCODE_PROCBEGIN) processing = true;
			Opcode currOp = (Opcode)opcodes[ReadByte];
			lastScanpoint = scanpoint;
			if (!condition && !reading)
			{
				scanpoint = currOp.Execute(this, scanpoint, false);
			}
			else if ((bool)registers["cond"] && !reading)
			{
				scanpoint = currOp.Execute(this, scanpoint, false);
			}
			else if (ReadByte == OPCODE_READFLOAT) scanpoint += 4;
			if (ReadByte == OPCODE_STREND && reading) reading = false;
			if (reading) registers["hold"] = (string)registers["hold"] + (char)ReadByte;
			if (ReadByte == OPCODE_STRBEGIN) { reading = true; registers["hold"] = ""; }
			if (ReadByte == OPCODE_CONDBEGIN) condition = true;
			else if (ReadByte == OPCODE_CONDEND) condition = false;
			scanpoint += 1;
		}
		if (export)
        {
			var byteExport = new List<byte>();
			for (int i = 0; i < 1800; i++)
            {
				byteExport.Add(accessor.ReadByte(i));
            }
			GD.Print("exported");
			System.IO.File.WriteAllBytes("export.stb", byteExport.ToArray());
		}
		routine = newRoutine;
		routineLoop = true;
	}

	public void Interpret()
	{
		var file = new Godot.File();
		file.Open(bytecodeFile, Godot.File.ModeFlags.Read);
		for(int x = 0; x <= 1800; x++)
        {
			byte currByte = file.Get8();
			if (currByte == OPCODE_EOF) break;
			routine.Add(currByte);
        }
		file.Close();
		if (routine.Count == 0) return;
		var condition = false;
		var reading = false;
		var processing = false;
		ArrayList newRoutine = new ArrayList();
		int lastByte = 4;
		int i = 4;
		while (i < routine.Count)
		{
			if ((byte)routine[i] == OPCODE_CONDBEGIN) condition = true;
			if ((byte)routine[i] == OPCODE_CONDEND) condition = false;
			if ((byte)routine[i] == OPCODE_STREND) reading = false;
			if (processing && lastByte != i - 1)
            {
				for(int j = lastByte+1; j < i; j++) 
				{
					newRoutine.Add(routine[j]);
				}
            }
			lastByte = i;
			if ((byte)routine[i] == OPCODE_PROCEND) processing = false;
			if (processing) newRoutine.Add(routine[i]);
			if (!condition && !reading)
			{
				Opcode currOp = (Opcode)opcodes[routine[i]];
				i = currOp.Execute(this, i, true);
			}
			else if (condition && (bool)registers["cond"] && !reading)
			{
				Opcode currOp = (Opcode)opcodes[routine[i]];
				i = currOp.Execute(this, i, true);
			}
			else if (reading)
			{
				char temp = (char)(byte)routine[i];
				registers["hold"] = (string)registers["hold"] + temp;
			}
			if ((byte)routine[i] == OPCODE_STRBEGIN) { reading = true; registers["hold"] = ""; }
			if ((byte)routine[i] == OPCODE_PROCBEGIN) processing = true;
			i += 1;
		}
		routine = newRoutine;
		//for (int y = 0; y < routine.Count; y++) GD.Print(routine[y]);
		routineLoop = true;
	}

	public void ComputeRoutine(ArrayList routine)
	{
		if (routine.Count == 0) return;
		var condition = false;
		var reading = false;
		int i = 0;
		while (i < routine.Count)
		{
			if ((byte)routine[i] == OPCODE_CONDBEGIN) condition = true;
			if ((byte)routine[i] == OPCODE_CONDEND) condition = false;
			if ((byte)routine[i] == OPCODE_STREND) reading = false;
			if (!condition && !reading)
			{
				Opcode currOp = (Opcode)opcodes[routine[i]];
				i = currOp.Execute(this, i, true);
			}
			else if (condition && (bool)registers["cond"] && !reading)
			{
				Opcode currOp = (Opcode)opcodes[routine[i]];
				i = currOp.Execute(this, i, true);
			}
			else if (reading)
			{
				char temp = (char)(byte)routine[i];
				registers["hold"] = (string)registers["hold"] + temp;
			}
			if ((byte)routine[i] == OPCODE_STRBEGIN) { reading = true; registers["hold"] = ""; }
			i += 1;
		}
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(float delta)
	{
		deltaTime = delta;

		if (!interpret)
		{
			if (accessor.ReadByte(0) != 0)
			{
				var jump = accessor.ReadByte(0);
				accessor.Write(0, (byte)0);
				Compute(jump);
			}
		}
		if (routineLoop) 
		{
			//if (debug) for(int i = 0; i < routine.Count; i++) GD.Print(routine[i]);
			ComputeRoutine(routine);
		}
			
	}

	public override void _Notification(int what)
	{
		if (what == MainLoop.NotificationWmQuitRequest)
		{
			accessor.Dispose();
			mmf.Dispose();
			GetTree().Quit(); // default behavior
		}
	}

	public byte ReadMemory(int scanpoint, bool read)
	{
		if (!read)
		{
			return accessor.ReadByte(scanpoint);
		}
		else
		{
			return (byte)routine[scanpoint];
		}
	}

	public float ReadFloat(int scanpoint, bool read)
	{
		if (!read)
		{
			return accessor.ReadSingle(scanpoint);
		}
		else
		{
			byte[] toConvert = { (byte)routine[scanpoint], (byte)routine[scanpoint + 1], (byte)routine[scanpoint + 2], (byte)routine[scanpoint + 3] };
			return BitConverter.ToSingle(toConvert, 0);
		}
	}

	public abstract class Opcode : object
	{
		public abstract int Execute(SmalltalkVM vm, int scanpoint, bool read);
	}
	public class Op_WriteIdentityRegisterHold : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read)
		{
			vm.registers["hold"] = vm.identities[vm.ReadMemory(scanpoint+1, read)];
			return (scanpoint + 1);
		}
	}

	public class Op_WriteFloatRegisterHold : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read)
		{
			vm.registers["hold"] = vm.ReadFloat(scanpoint + 1, read);
			return (scanpoint + 4);
		}
	}

	public class Op_Null : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read)
		{
			return scanpoint;
		}
	}

	public class Op_WriteRegister1 : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read)
		{
			vm.registers[(byte)1] = vm.registers["hold"];
			if (vm.debug) GD.Print("Register 1 set to " + vm.registers["hold"]);
			return (scanpoint);
		}
	}
	public class Op_WriteRegister2 : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read)
		{
			vm.registers[(byte)2] = vm.registers["hold"];
			if (vm.debug) GD.Print("Register 2 set to " + vm.registers["hold"]);
			return (scanpoint);
		}
	}

	public class Op_WriteRegister3 : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read)
		{
			vm.registers[(byte)3] = vm.registers["hold"];
			if (vm.debug) GD.Print("Register 3 set to " + vm.registers["hold"]);
			return (scanpoint);
		}
	}

	public class Op_WriteRegister4 : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read)
		{
			vm.registers[(byte)4] = vm.registers["hold"];
			if (vm.debug) GD.Print("Register 4 set to " + vm.registers["hold"]);
			return (scanpoint);
		}
	}

	public class Op_WriteRegister5 : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read)
		{
			vm.registers[(byte)5] = vm.registers["hold"];
			if (vm.debug) GD.Print("Register 5 set to " + vm.registers["hold"]);
			return (scanpoint);
		}
	}

	public class Op_StringBegin : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read)
		{
			return scanpoint;
		}
	}

	public class Op_StringEnd : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read)
		{
			return scanpoint;
		}
	}

	public class Op_ConditionalBegin : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read)
		{
			return scanpoint;
		}
	}

	public class Op_WriteCondRegister : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read)
		{
			//GD.Print(vm.registers["hold"]);
			vm.registers["cond"] = (bool)vm.registers["hold"];
			if (vm.debug) GD.Print("Cond Register set to " + vm.registers["hold"]);
			return (scanpoint);
		}
	}

	public class Op_ConditionalEnd : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read)
		{
			return scanpoint;
		}
	}

	public class Op_GreaterThan : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read)
		{
			vm.registers["hold"] = (float)vm.registers[(byte)1] > (float)vm.registers[(byte)2];
			return scanpoint;
		}
	}

	public class Op_GreaterThanEqual : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read)
		{
			vm.registers["hold"] = (float)vm.registers[(byte)1] >= (float)vm.registers[(byte)2];
			return scanpoint;
		}
	}
	public class Op_LessThan : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read)
		{
			vm.registers["hold"] = (float)vm.registers[(byte)1] < (float)vm.registers[(byte)2];
			return scanpoint;
		}
	}

	public class Op_LessThanEqual : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read)
		{
			vm.registers["hold"] = (float)vm.registers[(byte)1] <= (float)vm.registers[(byte)2];
			return scanpoint;
		}
	}

	public class Op_Equal : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read)
		{
			vm.registers["hold"] = vm.registers[(byte)1] == vm.registers[(byte)2];
			return scanpoint;
		}
	}

	public class Op_NotEqual : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read)
		{
			vm.registers["hold"] = vm.registers[(byte)1] != vm.registers[(byte)2];
			return scanpoint;
		}
	}

	public class Op_Print : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read)
		{
			GD.Print(vm.registers[(byte)1]);
			return scanpoint;
		}
	}
	public class Op_StartRecordProcess : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read)
		{
			return scanpoint;
		}
	}
	public class Op_StopRecordProcess : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read)
		{
			return scanpoint;
		}
	}
	public class Op_EndOfFile : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read)
		{
			return scanpoint;
		}
	}

	public class Op_SetIdentifier : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read)
		{
			vm.identities[(byte)vm.registers[(byte)1]] = vm.registers[(byte)2];
			return scanpoint;
		}
	}

	public class Op_Add : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read)
		{
			vm.registers["hold"] = (float)vm.registers[(byte)1] + (float)vm.registers[(byte)2];
			return scanpoint;
		}
	}

	public class Op_Subtract : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read)
		{
			vm.registers["hold"] = (float)vm.registers[(byte)1] - (float)vm.registers[(byte)2];
			return scanpoint;
		}
	}

	public class Op_ReadByte : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read)
		{
			vm.registers["hold"] = vm.ReadMemory(scanpoint + 1, read);
			return (scanpoint + 1);
		}
	}

	public class Op_MoveAndSlide : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read)
		{
			//GD.Print("register 1 before sliding");
			//GD.Print(vm.registers[(byte)1]);
			var r1 = (KinematicBody)vm.registers[(byte)1];
			var r2 = vm.registers[(byte)2];
			var r3 = vm.registers[(byte)3];
			var r4 = vm.registers[(byte)4];

			var vec = new Vector3((float)r2*vm.deltaTime, (float)r3*vm.deltaTime, (float)r4*vm.deltaTime);

			r1.MoveAndSlide(vec);
			if (vm.debug) GD.Print("MoveAndSlide success");
			return scanpoint;

		}
	}

	public class Op_IsPressed : Opcode
	{
		public override int Execute(SmalltalkVM vm, int scanpoint, bool read)
		{
			//GD.Print("Register 1 after reading string");
			//GD.Print((string)vm.registers[(byte)1]);
			vm.registers["hold"] = Input.IsActionPressed((string)vm.registers[(byte)1]);
			//GD.Print("Hold register after reading bool");
			//GD.Print(vm.registers["hold"]);
			return scanpoint;
		}
	}
}

